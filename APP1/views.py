from django.shortcuts import render
from .models import Investigacion

# Create your views here.
def index(request):
    return render(request, "index.html",{})

def ListInv(request):
    context={
    "inv": Investigacion.objects.all()
    }
    return render(request,"ListInv.html",context)
