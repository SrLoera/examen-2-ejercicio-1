from django.db import models
from django.conf import settings
from django import forms


# Create your models here.


class Investigacion(models.Model):
	Tipo_inv=[
	('computacion','Computacion'),
	('tierra','Tierra'),
	('naturales','Naturales'),
	('sociales','Sociales'),
	('medicas','Medicas'),
	]
	autor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1)
	nombre_investigacion = models.CharField(max_length=100)
	tipo_investigacion = models.CharField(max_length=100,choices=Tipo_inv)

	def __str__(self):
		return self.nombre_investigacion
